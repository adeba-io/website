const DATE_ID = "#nav-date";
const TIME_ID = "#nav-time";

const MONTHS = [
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

var dateDiv, timeDiv;

function updateTime() {
	var d = new Date();
	timeDiv.textContent = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
}

function start() {
	dateDiv = document.querySelector(DATE_ID);
	timeDiv = document.querySelector(TIME_ID);

	var d = new Date();
	dateDiv.textContent = d.toDateString();

	updateTime();
	window.setInterval(updateTime, 1000);
}

if (window.addEventListener)
	window.addEventListener("load", start);
